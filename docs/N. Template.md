# N. Problem name

- Difficulty: Easy / Medium / Hard
- Topics: `Topic1`, `Topic2`
- Link: {{ link to the problem }}

## Description

Problem description goes here.

## Solution

### Solution 1

1st solution goes here.

```lang
1st solution code goes here.
```

### Solution 2

2nd solution goes here.

```lang
2nd solution code goes here.
```
