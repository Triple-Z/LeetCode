<!-- omit in toc -->
# LeetCode

LeetCode 练习仓库。

- [Problems](#problems)
  - [Algorithms](#algorithms)
- [License](#license)

## Problems

### Algorithms

|#| Title|Solution|Difficulty| Topics | Doc |
|:----:|:----:|:----:|:----:|:----: | :----: |
|1 | Two Sum | [Python3](py3/1.py), [C++](cpp/src/1.cpp), [Java](java/src/TwoSum.java) | Easy| `Array` , `Hash Table` | [:page_facing_up:](docs/1.%20Two%20Sum.md) |
|2 |Add Two Numbers |[Python3](py3/2.py)| Medium|
|3 |Longest Substring Without Repeating Characters | [Python3](py3/3.py) | Medium |
|4| Median of Two Sorted Arrays |[Python3](py3/4.py)|Hard|
|5| Longest Palindromic Substring | [Python3](py3/5.py) | Medium|
|6| ZigZag Conversion |[Python3](py3/6.py) | Medium|
|7| Reverse Integer<br>整数反转 | [Python3](py3/7.py), [Java](java/src/ReverseInteger.java) | Easy | `Math` | [:page_facing_up:](docs/7.%20Reverse%20Integer%20整数反转.md) |
|8| String to Integer<br>字符串转换整数(atoi) | [Java](java/src/StringToInteger.java) | Medium | `Math` , `String` | [:page_facing_up:](docs/8.%20String%20to%20Integer%20字符串转换整数(atoi).md) |
|9| Palindrome Number | [Python3](py3/9.py) | Easy |
|10| Regular Expression Matching | [Python3](py3/10.py) | Hard |
|11| Container With Most Water | [C++](cpp/src/11.cpp) | Medium | `Array` , `Two Pointers` | [:page_facing_up:](docs/11.%20Container%20With%20Most%20Water.md) |
|13| Roman to Integer | [Python3](py3/13.py) | Easy |
|14| Longest Common Prefix | [Python3](py3/14.py) | Easy |
|15| 3Sum | [C++](cpp/src/15.cpp) | Medium | `Array` , `Two Pointers` | [:page_facing_up:](docs/15.%203Sum.md) |
|16| 3Sum Closest | [C++](cpp/src/16.cpp) | Medium | `Array` , `Two Pointers` | [:page_facing_up:](docs/16.%203Sum%20Closest.md) |
|18| 4Sum | [C++](cpp/src/18.cpp) | Medium | `Array` , `Hash Table` , `Two Pointers` | [:page_facing_up:](docs/18.%204Sum.md) |
|20| Valid Parentheses | [Python3](py3/20.py) | Easy |
|21| Merge Two Sorted Lists | [Python3](py3/21.py) | Easy |
|22| Generate Parentheses | [Python3](py3/22.py) | Easy |
|26| Remove Duplicates from Sorted Array | [C++](cpp/src/26.cpp) | Easy | `Array` , `Two Pointers` | [:page_facing_up:](docs/26.%20Remove%20Duplicates%20from%20Sorted%20Array.md) |
|27| Remove Element | [C++](cpp/src/27.cpp) | Easy | `Array` , `Two Pointers` | [:page_facing_up:](docs/27.%20Remove%20Element.md) |
|35| Search Insert Position | [Python3](py3/35.py), [C++](cpp/src/35.cpp), [Java](java/src/SearchInsertPosition.java) | Easy | `Array` , `Binary Search` | [:page_facing_up:](docs/35.%20Search%20Insert%20Position.md) |
|36| Valid Sudoku<br>有效的数独 | [Java](java/src/ValidSudoku.java) | Medium | `Hash Table` | [:page_facing_up:](docs/36.%20Valid%20Sudoku%20有效的数独.md) |
|48| Rotate Image<br>旋转图像 | [Java](java/src/RotateImage.java) | Medium | `Array` | [:page_facing_up:](docs/48.%20Rotate%20Image%20旋转图像.md) |
|53| Maximum Subarray | [Python3](py3/53.py), [C++](cpp/src/53.cpp) | Easy | `Array` , `Divide and Conquer` , `Dynamic Programming` | [:page_facing_up:](docs/53.%20Maximum%20Subarray.md) |
|66| Plus One | [Python3](py3/66.py), [C++](cpp/src/66.cpp) | Easy | `Array` , `Math` | [:page_facing_up:](docs/66.%20Plus%20One.md)
|67| Add Binary | [Python3](py3/67.py) | Easy |
|69| Sqrt(x) | [Python3](py3/69.py) | Easy |
|70| Climbing Stairs | [Python3](py3/70.py) | Easy |
|80| Remove Duplicates from Sorted Array II | [C++](cpp/src/80.cpp) | Medium | `Array` , `Two Pointers` | [:page_facing_up:](docs/80.%20Remove%20Duplicates%20from%20Sorted%20Array%20II.md) |
|88| Merge Sorted Array | [C++](cpp/src/88.cpp) | Easy | `Array` , `Two Pointers` | [:page_facing_up:](docs/88.%20Merge%20Sorted%20Array.md) |
|94| Binary Tree Inorder Traversal | [Python3](py3/94.py) | Medium |
|100| Same Tree | [Python3](py3/100.py) | Easy | `DFS` |
|101| Symmetric Tree | [Python3](py3/101.py) | Easy |
|104| Maximum Depth of Binary Tree | [Python3](py3/104.py) | Easy |
|121| Best Time to Buy and Sell Stock | [Python3](py3/121.py), [C++](cpp/src/121.cpp) | Easy | `Array` , `Dynamic Programming` | [:page_facing_up:](docs/121.%20Best%20Time%20to%20Buy%20and%20Sell%20Stock.md) |
|122| Best Time to Buy and Sell Stock II | [C++](cpp/src/122.cpp) | Easy | `Array` , `Greedy` | [:page_facing_up:](docs/122.%20Best%20Time%20to%20Buy%20and%20Sell%20Stock%20II.md) |
|123| Best Time to Buy and Sell Stock III | [C++](cpp/src/123.cpp) | Easy | `Array` , `Dynamic Programming` | [:page_facing_up:](docs/123.%20Best%20Time%20to%20Buy%20and%20Sell%20Stock%20III.md) |
|125| Valid PalinDrome<br>验证回文字符串 | [Java](java/src/ValidPaliDrome.java) | Easy | `Two Pointers` , `String` | [:page_facing_up:](docs/125.%20Valid%20PalinDrome%20验证回文串.md) |
|136| Single Number | [Python3](py3/136.py) | Easy |
|141| Linked List Cycle | [Python3](py3/141.py) | Easy |
|155| Min Stack | [Python3](py3/155.py) | Easy |
|160| Intersection of Two Linked Lists | [Python3](py3/160.py) | Easy |
|169| Majority Element | [Python3](py3/169.py) | Easy |
|198| House Robber | [Python3](py3/198.py) | Easy |
|206| Reverse Linked List | [Python3](py3/206.py) | Easy |
|226| Invert Binary Tree | [Python3](py3/226.py) | Easy |
|234| Palindrome Linked List | [Python3](py3/234.py) | Easy |
|238| Product of Array Except Self | [Python3](py3/238.py) | Medium |
|242| Valid Anagram<br>有效的字母异位词 | [Java](java/src/ValidAnagram.java) | Easy | `Sort`, `Hash Table` | [:page_facing_up:](docs/242.%20Valid%20Anagram%20有效的字母异位词.md) |
|283| Move Zeroes | [Python3](py3/283.py) | Easy |
|338| Counting Bits | [Python3](py3/338.py) | Medium |
|344| Reverse String<br>反转字符串 | [Java](java/src/ReverseString.java) | Easy | `Two Pointers` , `String` | [:page_facing_up:](docs/344.%20Reverse%20String%20反转字符串.md) |
|347| Top K Frequent Elements | [Python3](py3/347.py) | Medium |
|387| First Unique Character in a String<br>字符串中第一个唯一字符 | [Java](java/src/FirstUniqueCharacterInAString.java) | Easy | `Hash Table`, `String` | [:page_facing_up:](docs/387.%20First%20Unique%20Character%20in%20a%20String%20字符串中第一个唯一字符.md) |
|406| Queue Reconstruction by Height | [Python3](py3/406.py) | Medium |
|437| Path Sum III | [Python3](py3/437.py) | Easy |
|438| Find All Anagrams in a String | [Python3](py3/438.py) | Easy |
|448| Find All Numbers Disappeared in an Array | [Python3](py3/448.py) | Easy |
|538| Convert BST to Greater Tree | [Python3](py3/538.py) | Easy |
|543| Diameter of Binary Tree | [Python3](py3/543.py) | Easy |
|572| Subtree of Another Tree | [Python3](py3/572.py) | Easy |
|581| Shortest Unsorted Continuous Subarray | [Python3](py3/581.py) | Easy |
|617| Merge Two Binary Trees | [Python3](py3/617.py) | Easy |
|647| Palindromic Substrings | [Python3](py3/647.py) | Medium |
|697| Degree of an Array | [C++](cpp/src/697.cpp) | Easy | `Array` | [:page_facing_up:](docs/697.%20Degree%20of%20an%20Array.md) |
|832| Flipping an Image | [C++](cpp/src/832.cpp) | Easy | `Array` |
|905| Sort Array By Parity | [C++](cpp/src/905.cpp) | Easy | `Array` |
|1114| Print in Order<br>按序打印 | [Java](java/src/1114.%20PrintInOrder.java) | Easy | | [:page_facing_up:](docs/1114.%20Print%20in%20Order%20按序打印.md) |

## License

This repository is licensed under the [MIT License](LICENSE).

``` 
MIT License

Copyright (c) 2018-2020 Zhenzhen Zhao

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
